artifacts.require('./Imports.sol');

const AutoBuyBack = artifacts.require('./AutoBuyBack.sol');
const GaltToken = artifacts.require('openzeppelin-solidity/contracts/token/ERC20/ERC20Mintable.sol');
const MockDex = artifacts.require('./MockDex.sol');

const { web3 } = AutoBuyBack;

contract('AutoBuyBack', ([coreTeam, alice, bob]) => {
  beforeEach(async function() {
    this.galtToken = await GaltToken.new();
    this.mockDex = await MockDex.new(this.galtToken.address);
    this.autoBuyBack = await AutoBuyBack.new(this.mockDex.address, { from: coreTeam });
    await this.galtToken.mint(this.mockDex.address, web3.utils.toWei('42', 'ether'));
    assert.equal(await this.galtToken.balanceOf(this.mockDex.address), web3.utils.toWei('42', 'ether'));
  });

  describe('#mint()', () => {
    it('should allow sending eths to contract', async function() {
      await this.autoBuyBack.sendTransaction({ from: alice, value: web3.utils.toWei('200', 'ether') });
      assert.equal(await web3.eth.getBalance(this.autoBuyBack.address), web3.utils.toWei('200', 'ether'));

      await this.autoBuyBack.swap({ from: bob });

      assert.equal(await web3.eth.getBalance(this.mockDex.address), web3.utils.toWei('200', 'ether'));
      assert.equal(await this.galtToken.balanceOf(this.autoBuyBack.address), web3.utils.toWei('42', 'ether'));

      await this.autoBuyBack.swap({ from: alice });

      assert.equal(await web3.eth.getBalance(this.mockDex.address), web3.utils.toWei('200', 'ether'));
      assert.equal(await this.galtToken.balanceOf(this.autoBuyBack.address), web3.utils.toWei('42', 'ether'));
    });
  });
});

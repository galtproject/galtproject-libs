/*
 * Copyright ©️ 2018 Galt•Project Society Construction and Terraforming Company
 * (Founded by [Nikolai Popeka](https://github.com/npopeka)
 *
 * Copyright ©️ 2018 Galt•Core Blockchain Company
 * (Founded by [Nikolai Popeka](https://github.com/npopeka) by
 * [Basic Agreement](ipfs/QmaCiXUmSrP16Gz8Jdzq6AJESY1EAANmmwha15uR3c1bsS)).
 */

pragma solidity ^0.5.13;

import "@openzeppelin/contracts/access/Roles.sol";
import "./interfaces/IPermissionable.sol";


contract Permissionable is IPermissionable {
  using Roles for Roles.Role;

  event RoleAdded(address indexed account, string role);
  event RoleRemoved(address indexed account, string role);

  mapping (string => Roles.Role) private roles;

  string public constant ROLE_ROLE_MANAGER = "role_manager";

  constructor() public {
    _addRoleTo(msg.sender, ROLE_ROLE_MANAGER);
  }

  modifier onlyRole(string memory _role) {
    require(roles[_role].has(msg.sender), "Invalid role");

    _;
  }

  function hasRole(address _account, string memory _role) public view returns (bool) {
    return roles[_role].has(_account);
  }

  function requireRole(address _account, string memory _role) public view {
    require(roles[_role].has(_account), "Invalid role");
  }

  function addRoleTo(address _account, string memory _role) public onlyRole(ROLE_ROLE_MANAGER) {
    _addRoleTo(_account, _role);
  }

  function removeRoleFrom(address _account, string memory _role) public onlyRole(ROLE_ROLE_MANAGER) {
    _removeRoleFrom(_account, _role);
  }

  function _addRoleTo(address _account, string memory _role) internal {
    roles[_role].add(_account);
    emit RoleAdded(_account, _role);
  }

  function _removeRoleFrom(address _account, string memory _role) internal {
    roles[_role].remove(_account);
    emit RoleRemoved(_account, _role);
  }
}

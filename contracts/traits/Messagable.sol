/*
 * Copyright ©️ 2018 Galt•Project Society Construction and Terraforming Company
 * (Founded by [Nikolai Popeka](https://github.com/npopeka)
 *
 * Copyright ©️ 2018 Galt•Core Blockchain Company
 * (Founded by [Nikolai Popeka](https://github.com/npopeka) by
 * [Basic Agreement](ipfs/QmaCiXUmSrP16Gz8Jdzq6AJESY1EAANmmwha15uR3c1bsS)).
 */

pragma solidity ^0.5.13;

import "../collections/ArraySet.sol";


contract Messagable {
  event NewMessage(address indexed account);

  using ArraySet for ArraySet.AddressSet;

  struct Message {
    uint256 id;
    uint256 timestamp;
    address from;
    string text;
  }

  struct MessagesList {
    uint256 count;
    Message[] messages;
  }

  // applicationId => MessagesList
  mapping(bytes32 => MessagesList) messages;

  modifier onlyApplicationParticipant(bytes32 _aId) {
    // sender could be either an applicant or any oracle who locked the application
    // WARNING: should be overridden
    assert(false);
    _;
  }

  function pushMessage(bytes32 _aId, string calldata _text) external;
}

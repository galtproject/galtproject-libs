/*
 * Copyright ©️ 2018 Galt•Project Society Construction and Terraforming Company
 * (Founded by [Nikolai Popeka](https://github.com/npopeka)
 *
 * Copyright ©️ 2018 Galt•Core Blockchain Company
 * (Founded by [Nikolai Popeka](https://github.com/npopeka) by
 * [Basic Agreement](ipfs/QmaCiXUmSrP16Gz8Jdzq6AJESY1EAANmmwha15uR3c1bsS)).
 */

pragma solidity ^0.5.13;


/**
 * @title Initializable
 * @dev Simple helper contract to support initialization outside of the constructor.
 * To use it, replace the constructor with a function that has the
 * `isInitializer` modifier.
 * WARNING: This helper does not support multiple inheritance.
 * WARNING: It is the developer's responsibility to ensure that an initializer
 * is actually called.
 * Use `Migratable` for more complex migration mechanisms.
 */
contract Initializable {

  /**
   * @dev Indicates if the contract has been initialized.
   */
  bool public initialized;

  /**
   * @dev Modifier to use in the initialization function of a contract.
   */
  modifier isInitializer() {
    require(!initialized, "Contract instance has already been initialized");
    _;
    initialized = true;
  }
}

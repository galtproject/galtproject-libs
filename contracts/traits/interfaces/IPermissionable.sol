/*
 * Copyright ©️ 2018 Galt•Project Society Construction and Terraforming Company
 * (Founded by [Nikolai Popeka](https://github.com/npopeka)
 *
 * Copyright ©️ 2018 Galt•Core Blockchain Company
 * (Founded by [Nikolai Popeka](https://github.com/npopeka) by
 * [Basic Agreement](ipfs/QmaCiXUmSrP16Gz8Jdzq6AJESY1EAANmmwha15uR3c1bsS)).
 */

pragma solidity ^0.5.13;


interface IPermissionable {
  function hasRole(address _account, string calldata _role) external view returns (bool);
  function requireRole(address _account, string calldata _role) external view;
  function addRoleTo(address _account, string calldata _role) external;
  function removeRoleFrom(address _account, string calldata _role) external;
}

/*
 * Copyright ©️ 2018 Galt•Project Society Construction and Terraforming Company
 * (Founded by [Nikolai Popeka](https://github.com/npopeka)
 *
 * Copyright ©️ 2018 Galt•Core Blockchain Company
 * (Founded by [Nikolai Popeka](https://github.com/npopeka) by
 * [Basic Agreement](ipfs/QmaCiXUmSrP16Gz8Jdzq6AJESY1EAANmmwha15uR3c1bsS)).
 */

pragma solidity ^0.5.13;


library ArrayUtils {
  function uintSome(uint[] memory arr, uint el) internal pure returns (bool) {
    for (uint j = 0; j < arr.length; j++) {
      if (el == arr[j]) {
        return true;
      }
    }
    return false;
  }

  function uintFind(uint[] memory arr, uint el) internal pure returns (int) {
    for (uint j = 0; j < arr.length; j++) {
      if (el == arr[j]) {
        return int(j);
      }
    }
    return - 1;
  }

  function intEqual(int[] memory arr1, int[] memory arr2) internal pure returns (bool) {
    for (uint i = 0; i < arr1.length; i++) {
      if (arr1[i] != arr2[i]) {
        return false;
      }
    }
    return true;
  }
}

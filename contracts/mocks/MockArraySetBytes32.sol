/*
 * Copyright ©️ 2018 Galt•Project Society Construction and Terraforming Company
 * (Founded by [Nikolai Popeka](https://github.com/npopeka)
 *
 * Copyright ©️ 2018 Galt•Core Blockchain Company
 * (Founded by [Nikolai Popeka](https://github.com/npopeka) by
 * [Basic Agreement](ipfs/QmaCiXUmSrP16Gz8Jdzq6AJESY1EAANmmwha15uR3c1bsS)).
 */

pragma solidity ^0.5.13;

import "../collections/ArraySet.sol";


contract MockArraySetBytes32 {
  using ArraySet for ArraySet.Bytes32Set;
  ArraySet.Bytes32Set set;

  // MODIFIERS
  function add(bytes32 _v) external {
    set.add(_v);
  }

  function remove(bytes32 _v) external {
    set.remove(_v);
  }

  function clear() external {
    set.clear();
  }

  // GETTERS
  function has(bytes32 _v) external view returns (bool) {
    return set.has(_v);
  }

  function elements() external view returns (bytes32[] memory) {
    return set.elements();
  }

  function size() external view returns (uint256) {
    return set.size();
  }

  function isEmpty() external view returns (bool) {
    return set.isEmpty();
  }
}
